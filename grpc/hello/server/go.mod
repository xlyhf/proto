module server

go 1.15

require (
	gitee.com/xlyhf/proto v0.0.0-20210320141653-a03481e63777 // indirect
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.26.0
)
